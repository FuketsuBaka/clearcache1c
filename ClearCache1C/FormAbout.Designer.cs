﻿namespace ClearCache1C
{
    partial class FormAbout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAbout));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBoxAi = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lInfo = new System.Windows.Forms.Label();
            this.labelVersion = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.linkGit = new System.Windows.Forms.LinkLabel();
            this.linkContact = new System.Windows.Forms.LinkLabel();
            this.labelYear = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAi)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBoxAi);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(357, 136);
            this.panel1.TabIndex = 0;
            // 
            // pictureBoxAi
            // 
            this.pictureBoxAi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxAi.Image = global::ClearCache1C.Properties.Resources.ai_logo;
            this.pictureBoxAi.InitialImage = null;
            this.pictureBoxAi.Location = new System.Drawing.Point(4, 4);
            this.pictureBoxAi.Name = "pictureBoxAi";
            this.pictureBoxAi.Size = new System.Drawing.Size(142, 130);
            this.pictureBoxAi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxAi.TabIndex = 0;
            this.pictureBoxAi.TabStop = false;
            this.pictureBoxAi.Click += new System.EventHandler(this.pictureBoxAi_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lInfo);
            this.panel2.Controls.Add(this.labelVersion);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(152, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(205, 136);
            this.panel2.TabIndex = 1;
            // 
            // lInfo
            // 
            this.lInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.lInfo.ForeColor = System.Drawing.Color.DimGray;
            this.lInfo.Location = new System.Drawing.Point(0, 47);
            this.lInfo.Name = "lInfo";
            this.lInfo.Size = new System.Drawing.Size(205, 85);
            this.lInfo.TabIndex = 1;
            this.lInfo.Text = "Все просто.\r\nПрограмма читает пользовательский файл ibases.v8i и на его основе ст" +
    "роит список баз.\r\nПо найденным ID и происходит вся магия удалений.";
            this.lInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelVersion
            // 
            this.labelVersion.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelVersion.ForeColor = System.Drawing.Color.DarkRed;
            this.labelVersion.Location = new System.Drawing.Point(0, 24);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(205, 23);
            this.labelVersion.TabIndex = 3;
            this.labelVersion.Text = "Версия: 1.0.0.0";
            this.labelVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(205, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "Чистка Кэша 1С";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // linkGit
            // 
            this.linkGit.Dock = System.Windows.Forms.DockStyle.Top;
            this.linkGit.LinkArea = new System.Windows.Forms.LinkArea(5, 46);
            this.linkGit.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkGit.LinkColor = System.Drawing.Color.DarkRed;
            this.linkGit.Location = new System.Drawing.Point(0, 166);
            this.linkGit.Name = "linkGit";
            this.linkGit.Size = new System.Drawing.Size(357, 30);
            this.linkGit.TabIndex = 2;
            this.linkGit.TabStop = true;
            this.linkGit.Text = "GIT: https://bitbucket.org/FuketsuBaka/clearcache1c";
            this.linkGit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.linkGit.UseCompatibleTextRendering = true;
            this.linkGit.VisitedLinkColor = System.Drawing.SystemColors.WindowFrame;
            this.linkGit.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkGit_LinkClicked);
            // 
            // linkContact
            // 
            this.linkContact.Dock = System.Windows.Forms.DockStyle.Top;
            this.linkContact.LinkArea = new System.Windows.Forms.LinkArea(20, 15);
            this.linkContact.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkContact.LinkColor = System.Drawing.Color.DarkRed;
            this.linkContact.Location = new System.Drawing.Point(0, 136);
            this.linkContact.Name = "linkContact";
            this.linkContact.Size = new System.Drawing.Size(357, 30);
            this.linkContact.TabIndex = 3;
            this.linkContact.TabStop = true;
            this.linkContact.Text = "Автор: FuketsuBaka (ai@ai-frame.net)";
            this.linkContact.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.linkContact.UseCompatibleTextRendering = true;
            this.linkContact.VisitedLinkColor = System.Drawing.SystemColors.WindowFrame;
            this.linkContact.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkContact_LinkClicked);
            // 
            // labelYear
            // 
            this.labelYear.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelYear.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.labelYear.Location = new System.Drawing.Point(0, 198);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(357, 21);
            this.labelYear.TabIndex = 4;
            this.labelYear.Text = "-2018 г.-";
            this.labelYear.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormAbout
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(357, 219);
            this.Controls.Add(this.labelYear);
            this.Controls.Add(this.linkGit);
            this.Controls.Add(this.linkContact);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormAbout";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "О программе";
            this.Load += new System.EventHandler(this.FormAbout_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAi)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBoxAi;
        private System.Windows.Forms.Label lInfo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel linkGit;
        private System.Windows.Forms.LinkLabel linkContact;
        private System.Windows.Forms.Label labelYear;
        private System.Windows.Forms.Label labelVersion;
    }
}