﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClearCache1C
{
    public partial class FormResults : Form
    {
        public FormResults()
        {
            InitializeComponent();
        }

        public void SetResults(int successfullyDeleted, int errorsOnDelete, double totalSizeDeleted,
            List<string> listSuccess, List<string> listError, List<string> listExceptions)
        {
            tbResults.ResetText();
            tbResults.AppendText("--- Итоги ---");
            tbResults.AppendText("\r\nВыполнено успешно: " + successfullyDeleted);
            tbResults.AppendText("\r\nВыполнено с ошибками: " + errorsOnDelete);
            tbResults.AppendText("\r\nВсего удалено: " + totalSizeDeleted.ToString("0.00") + " Mb");
            tbResults.AppendText("\r\n");
            tbResults.AppendText("\r\n--- Список обработанных успешно ---");
            foreach (string s in listSuccess)
                tbResults.AppendText("\r\n > " + s);
            tbResults.AppendText("\r\n");
            tbResults.AppendText("\r\n--- Список обработанных с ошибками ---");
            foreach (string s in listError)
            {
                tbResults.AppendText("\r\n > " + s);
            }
            tbResults.AppendText("\r\n");
            tbResults.AppendText("\r\n--- Лог ошибок ---");
            foreach (string s in listExceptions)
                tbResults.AppendText("\r\n > " + s);
        }
    }
}
