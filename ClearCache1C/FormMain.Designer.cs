﻿namespace ClearCache1C
{
    partial class FormMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.BaseDataView = new System.Windows.Forms.DataGridView();
            this.panBtnsBaseGrid = new System.Windows.Forms.Panel();
            this.cbInfo = new System.Windows.Forms.CheckBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnUnselect = new System.Windows.Forms.Button();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.btnSelectCache = new System.Windows.Forms.Button();
            this.btnAbout = new System.Windows.Forms.Button();
            this.panBtnsBaseSubmit = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbOptSettingsUser = new System.Windows.Forms.CheckBox();
            this.cbOptCacheProgram = new System.Windows.Forms.CheckBox();
            this.cbOptCacheUser = new System.Windows.Forms.CheckBox();
            this.cbOptAll = new System.Windows.Forms.CheckBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.tbInfo = new System.Windows.Forms.TextBox();
            this.panMain = new System.Windows.Forms.Panel();
            this.splitMain = new System.Windows.Forms.SplitContainer();
            this.panSearch = new System.Windows.Forms.Panel();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.lSearch = new System.Windows.Forms.Label();
            this.btnDropSearch = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.BaseDataView)).BeginInit();
            this.panBtnsBaseGrid.SuspendLayout();
            this.panBtnsBaseSubmit.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitMain)).BeginInit();
            this.splitMain.Panel1.SuspendLayout();
            this.splitMain.Panel2.SuspendLayout();
            this.splitMain.SuspendLayout();
            this.panSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // BaseDataView
            // 
            this.BaseDataView.AllowUserToAddRows = false;
            this.BaseDataView.AllowUserToDeleteRows = false;
            this.BaseDataView.AllowUserToOrderColumns = true;
            this.BaseDataView.AllowUserToResizeRows = false;
            this.BaseDataView.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.BaseDataView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.BaseDataView.ColumnHeadersHeight = 38;
            this.BaseDataView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.BaseDataView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BaseDataView.GridColor = System.Drawing.Color.LightGray;
            this.BaseDataView.Location = new System.Drawing.Point(0, 0);
            this.BaseDataView.MinimumSize = new System.Drawing.Size(400, 0);
            this.BaseDataView.Name = "BaseDataView";
            this.BaseDataView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.LightYellow;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.BaseDataView.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.BaseDataView.RowHeadersVisible = false;
            this.BaseDataView.RowHeadersWidth = 20;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.LightYellow;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            this.BaseDataView.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.BaseDataView.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.BaseDataView.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.LightYellow;
            this.BaseDataView.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.BaseDataView.RowTemplate.Height = 18;
            this.BaseDataView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.BaseDataView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.BaseDataView.ShowEditingIcon = false;
            this.BaseDataView.Size = new System.Drawing.Size(564, 411);
            this.BaseDataView.TabIndex = 0;
            // 
            // panBtnsBaseGrid
            // 
            this.panBtnsBaseGrid.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panBtnsBaseGrid.Controls.Add(this.cbInfo);
            this.panBtnsBaseGrid.Controls.Add(this.btnRefresh);
            this.panBtnsBaseGrid.Controls.Add(this.btnUnselect);
            this.panBtnsBaseGrid.Controls.Add(this.btnSelectAll);
            this.panBtnsBaseGrid.Controls.Add(this.btnSelectCache);
            this.panBtnsBaseGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this.panBtnsBaseGrid.Location = new System.Drawing.Point(0, 0);
            this.panBtnsBaseGrid.Name = "panBtnsBaseGrid";
            this.panBtnsBaseGrid.Padding = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.panBtnsBaseGrid.Size = new System.Drawing.Size(564, 35);
            this.panBtnsBaseGrid.TabIndex = 4;
            // 
            // cbInfo
            // 
            this.cbInfo.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbInfo.Dock = System.Windows.Forms.DockStyle.Right;
            this.cbInfo.FlatAppearance.BorderSize = 0;
            this.cbInfo.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.ButtonFace;
            this.cbInfo.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.cbInfo.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.cbInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbInfo.Image = global::ClearCache1C.Properties.Resources.arrows_16_R;
            this.cbInfo.Location = new System.Drawing.Point(534, 3);
            this.cbInfo.Name = "cbInfo";
            this.cbInfo.Size = new System.Drawing.Size(27, 32);
            this.cbInfo.TabIndex = 7;
            this.cbInfo.UseVisualStyleBackColor = true;
            this.cbInfo.CheckedChanged += new System.EventHandler(this.CbInfo_CheckedChanged);
            // 
            // btnRefresh
            // 
            this.btnRefresh.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnRefresh.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnRefresh.FlatAppearance.BorderSize = 0;
            this.btnRefresh.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnRefresh.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefresh.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnRefresh.Image = global::ClearCache1C.Properties.Resources.icon_refresh_24;
            this.btnRefresh.Location = new System.Drawing.Point(453, 3);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(36, 32);
            this.btnRefresh.TabIndex = 4;
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.BtnRefresh_Click);
            // 
            // btnUnselect
            // 
            this.btnUnselect.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnUnselect.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnUnselect.Image = ((System.Drawing.Image)(resources.GetObject("btnUnselect.Image")));
            this.btnUnselect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUnselect.Location = new System.Drawing.Point(303, 3);
            this.btnUnselect.Name = "btnUnselect";
            this.btnUnselect.Size = new System.Drawing.Size(150, 32);
            this.btnUnselect.TabIndex = 3;
            this.btnUnselect.Text = "Снять выделение";
            this.btnUnselect.UseVisualStyleBackColor = true;
            this.btnUnselect.Click += new System.EventHandler(this.BtnUnselect_Click);
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSelectAll.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnSelectAll.Image = global::ClearCache1C.Properties.Resources.icon_select_16;
            this.btnSelectAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSelectAll.Location = new System.Drawing.Point(153, 3);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(150, 32);
            this.btnSelectAll.TabIndex = 2;
            this.btnSelectAll.Text = "Выделить все";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            this.btnSelectAll.Click += new System.EventHandler(this.BtnSelectAll_Click);
            // 
            // btnSelectCache
            // 
            this.btnSelectCache.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSelectCache.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnSelectCache.Image = global::ClearCache1C.Properties.Resources.icon_smart_select_16;
            this.btnSelectCache.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSelectCache.Location = new System.Drawing.Point(3, 3);
            this.btnSelectCache.Name = "btnSelectCache";
            this.btnSelectCache.Size = new System.Drawing.Size(150, 32);
            this.btnSelectCache.TabIndex = 1;
            this.btnSelectCache.Text = "Выделить с кэшем";
            this.btnSelectCache.UseVisualStyleBackColor = true;
            this.btnSelectCache.Click += new System.EventHandler(this.BtnSelectCache_Click);
            // 
            // btnAbout
            // 
            this.btnAbout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAbout.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnAbout.FlatAppearance.BorderSize = 0;
            this.btnAbout.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAbout.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAbout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAbout.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAbout.ForeColor = System.Drawing.Color.DimGray;
            this.btnAbout.Location = new System.Drawing.Point(514, 3);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(47, 48);
            this.btnAbout.TabIndex = 14;
            this.btnAbout.Text = "?";
            this.btnAbout.UseVisualStyleBackColor = true;
            this.btnAbout.Click += new System.EventHandler(this.BtnAbout_Click);
            // 
            // panBtnsBaseSubmit
            // 
            this.panBtnsBaseSubmit.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panBtnsBaseSubmit.Controls.Add(this.panel1);
            this.panBtnsBaseSubmit.Controls.Add(this.btnAbout);
            this.panBtnsBaseSubmit.Controls.Add(this.cbOptAll);
            this.panBtnsBaseSubmit.Controls.Add(this.btnClear);
            this.panBtnsBaseSubmit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panBtnsBaseSubmit.Location = new System.Drawing.Point(0, 472);
            this.panBtnsBaseSubmit.Name = "panBtnsBaseSubmit";
            this.panBtnsBaseSubmit.Padding = new System.Windows.Forms.Padding(3);
            this.panBtnsBaseSubmit.Size = new System.Drawing.Size(564, 54);
            this.panBtnsBaseSubmit.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cbOptSettingsUser);
            this.panel1.Controls.Add(this.cbOptCacheProgram);
            this.panel1.Controls.Add(this.cbOptCacheUser);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(283, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(231, 48);
            this.panel1.TabIndex = 8;
            // 
            // cbOptSettingsUser
            // 
            this.cbOptSettingsUser.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbOptSettingsUser.Location = new System.Drawing.Point(0, 34);
            this.cbOptSettingsUser.Name = "cbOptSettingsUser";
            this.cbOptSettingsUser.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.cbOptSettingsUser.Size = new System.Drawing.Size(231, 17);
            this.cbOptSettingsUser.TabIndex = 13;
            this.cbOptSettingsUser.Text = "Настройки пользователя";
            this.cbOptSettingsUser.UseVisualStyleBackColor = true;
            // 
            // cbOptCacheProgram
            // 
            this.cbOptCacheProgram.Checked = true;
            this.cbOptCacheProgram.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbOptCacheProgram.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbOptCacheProgram.Location = new System.Drawing.Point(0, 17);
            this.cbOptCacheProgram.Name = "cbOptCacheProgram";
            this.cbOptCacheProgram.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.cbOptCacheProgram.Size = new System.Drawing.Size(231, 17);
            this.cbOptCacheProgram.TabIndex = 12;
            this.cbOptCacheProgram.Text = "Кэш приложения";
            this.cbOptCacheProgram.UseVisualStyleBackColor = true;
            // 
            // cbOptCacheUser
            // 
            this.cbOptCacheUser.Checked = true;
            this.cbOptCacheUser.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbOptCacheUser.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbOptCacheUser.Location = new System.Drawing.Point(0, 0);
            this.cbOptCacheUser.Name = "cbOptCacheUser";
            this.cbOptCacheUser.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.cbOptCacheUser.Size = new System.Drawing.Size(231, 17);
            this.cbOptCacheUser.TabIndex = 11;
            this.cbOptCacheUser.Text = "Кэш пользователя";
            this.cbOptCacheUser.UseVisualStyleBackColor = true;
            // 
            // cbOptAll
            // 
            this.cbOptAll.AutoSize = true;
            this.cbOptAll.Dock = System.Windows.Forms.DockStyle.Left;
            this.cbOptAll.Location = new System.Drawing.Point(223, 3);
            this.cbOptAll.Name = "cbOptAll";
            this.cbOptAll.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.cbOptAll.Size = new System.Drawing.Size(60, 48);
            this.cbOptAll.TabIndex = 10;
            this.cbOptAll.Text = "Все";
            this.cbOptAll.UseVisualStyleBackColor = true;
            // 
            // btnClear
            // 
            this.btnClear.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnClear.Location = new System.Drawing.Point(3, 3);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(220, 48);
            this.btnClear.TabIndex = 9;
            this.btnClear.Text = "Очистить";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // tbInfo
            // 
            this.tbInfo.BackColor = System.Drawing.SystemColors.Info;
            this.tbInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbInfo.Location = new System.Drawing.Point(0, 0);
            this.tbInfo.MinimumSize = new System.Drawing.Size(400, 4);
            this.tbInfo.Multiline = true;
            this.tbInfo.Name = "tbInfo";
            this.tbInfo.ReadOnly = true;
            this.tbInfo.Size = new System.Drawing.Size(400, 100);
            this.tbInfo.TabIndex = 6;
            // 
            // panMain
            // 
            this.panMain.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panMain.Controls.Add(this.splitMain);
            this.panMain.Controls.Add(this.panSearch);
            this.panMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panMain.Location = new System.Drawing.Point(0, 35);
            this.panMain.Name = "panMain";
            this.panMain.Size = new System.Drawing.Size(564, 437);
            this.panMain.TabIndex = 7;
            // 
            // splitMain
            // 
            this.splitMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitMain.Location = new System.Drawing.Point(0, 26);
            this.splitMain.MinimumSize = new System.Drawing.Size(550, 0);
            this.splitMain.Name = "splitMain";
            // 
            // splitMain.Panel1
            // 
            this.splitMain.Panel1.AutoScroll = true;
            this.splitMain.Panel1.Controls.Add(this.BaseDataView);
            // 
            // splitMain.Panel2
            // 
            this.splitMain.Panel2.Controls.Add(this.tbInfo);
            this.splitMain.Panel2Collapsed = true;
            this.splitMain.Size = new System.Drawing.Size(564, 411);
            this.splitMain.SplitterDistance = 282;
            this.splitMain.SplitterWidth = 6;
            this.splitMain.TabIndex = 7;
            // 
            // panSearch
            // 
            this.panSearch.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panSearch.Controls.Add(this.tbSearch);
            this.panSearch.Controls.Add(this.lSearch);
            this.panSearch.Controls.Add(this.btnDropSearch);
            this.panSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.panSearch.Location = new System.Drawing.Point(0, 0);
            this.panSearch.Name = "panSearch";
            this.panSearch.Padding = new System.Windows.Forms.Padding(3);
            this.panSearch.Size = new System.Drawing.Size(564, 26);
            this.panSearch.TabIndex = 1;
            // 
            // tbSearch
            // 
            this.tbSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbSearch.Location = new System.Drawing.Point(48, 3);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(486, 20);
            this.tbSearch.TabIndex = 5;
            this.tbSearch.TextChanged += new System.EventHandler(this.TbSearch_TextChanged);
            // 
            // lSearch
            // 
            this.lSearch.Dock = System.Windows.Forms.DockStyle.Left;
            this.lSearch.Location = new System.Drawing.Point(3, 3);
            this.lSearch.Name = "lSearch";
            this.lSearch.Size = new System.Drawing.Size(45, 20);
            this.lSearch.TabIndex = 1;
            this.lSearch.Text = "Поиск:";
            this.lSearch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnDropSearch
            // 
            this.btnDropSearch.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDropSearch.FlatAppearance.BorderSize = 0;
            this.btnDropSearch.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDropSearch.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDropSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDropSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnDropSearch.Image = global::ClearCache1C.Properties.Resources.x_mark_16;
            this.btnDropSearch.Location = new System.Drawing.Point(534, 3);
            this.btnDropSearch.Name = "btnDropSearch";
            this.btnDropSearch.Size = new System.Drawing.Size(27, 20);
            this.btnDropSearch.TabIndex = 6;
            this.btnDropSearch.UseVisualStyleBackColor = true;
            this.btnDropSearch.Click += new System.EventHandler(this.BtnDropSearch_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 526);
            this.Controls.Add(this.panMain);
            this.Controls.Add(this.panBtnsBaseSubmit);
            this.Controls.Add(this.panBtnsBaseGrid);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(550, 39);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Чистка кэша 1C";
            this.Load += new System.EventHandler(this.FormMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.BaseDataView)).EndInit();
            this.panBtnsBaseGrid.ResumeLayout(false);
            this.panBtnsBaseSubmit.ResumeLayout(false);
            this.panBtnsBaseSubmit.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panMain.ResumeLayout(false);
            this.splitMain.Panel1.ResumeLayout(false);
            this.splitMain.Panel2.ResumeLayout(false);
            this.splitMain.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitMain)).EndInit();
            this.splitMain.ResumeLayout(false);
            this.panSearch.ResumeLayout(false);
            this.panSearch.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView BaseDataView;
        private System.Windows.Forms.Button btnSelectCache;
        private System.Windows.Forms.Button btnSelectAll;
        private System.Windows.Forms.Button btnUnselect;
        private System.Windows.Forms.Panel panBtnsBaseGrid;
        private System.Windows.Forms.Panel panBtnsBaseSubmit;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.CheckBox cbOptSettingsUser;
        private System.Windows.Forms.CheckBox cbOptCacheProgram;
        private System.Windows.Forms.CheckBox cbOptCacheUser;
        private System.Windows.Forms.CheckBox cbOptAll;
        private System.Windows.Forms.TextBox tbInfo;
        private System.Windows.Forms.CheckBox cbInfo;
        private System.Windows.Forms.Panel panMain;
        private System.Windows.Forms.SplitContainer splitMain;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Panel panSearch;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Label lSearch;
        private System.Windows.Forms.Button btnDropSearch;
        private System.Windows.Forms.Button btnAbout;
        private System.Windows.Forms.Panel panel1;
    }
}

