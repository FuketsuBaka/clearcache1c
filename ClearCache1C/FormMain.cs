﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
// Using
using System.IO;

namespace ClearCache1C
{
    public partial class FormMain : Form
    {
        // *********************************************************************************************
        // LVARS START
        // Data table
        private BaseData baseData;
        private string[] V8iContent;
        // LVARS END
        // *********************************************************************************************

        // *********************************************************************************************
        // TABLE INIT START
        private void InitDataTable()
        {
            baseData = new BaseData();
        }
        private void InitDataGridView()
        {
            //BindingSource bs = new BindingSource();
            //bs.DataSource = BaseData;
            BaseDataView.DataSource = baseData.dataTable;
            BaseDataView.Columns[baseData.col_Index.ColumnName].Visible = false;
            BaseDataView.Columns[baseData.col_Id.ColumnName].Visible = false;
            BaseDataView.Columns[baseData.col_ConnectString.ColumnName].Visible = false;
            BaseDataView.Columns[baseData.col_LastEnteredUser.ColumnName].Visible = false;

            // Formatting
            BaseDataView.Columns[baseData.col_SizeCacheUser.ColumnName].DefaultCellStyle.Format = "0.00";
            BaseDataView.Columns[baseData.col_SizeCacheProgram.ColumnName].DefaultCellStyle.Format = "0.00";
            BaseDataView.Columns[baseData.col_SizeTotal.ColumnName].DefaultCellStyle.Format = "0.00";

            // Columns formating
            BaseDataView.Columns[baseData.col_Check.ColumnName].Width = 22;
            BaseDataView.Columns[baseData.col_Check.ColumnName].ToolTipText = "Выбрано.";

            BaseDataView.Columns[baseData.col_Locked.ColumnName].Visible = false;
            BaseDataView.Columns[baseData.col_Locked.ColumnName].Width = 22;
            BaseDataView.Columns[baseData.col_Locked.ColumnName].ToolTipText = "Заблокировано.";

            BaseDataView.Columns[baseData.col_BaseName.ColumnName].Width = 200;
            BaseDataView.Columns[baseData.col_SizeCacheUser.ColumnName].Width = 55;
            BaseDataView.Columns[baseData.col_SizeCacheProgram.ColumnName].Width = 55;
            BaseDataView.Columns[baseData.col_SizeTotal.ColumnName].Width = 55;

            // Events handling
            BaseDataView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.BaseDataView_CellFormatting);
            BaseDataView.CellPainting += new DataGridViewCellPaintingEventHandler(this.BaseDataView_CellPainting);
            //deprecated
            //BaseDataView.CellValueChanged += new DataGridViewCellEventHandler(this.BaseDataView_ValueChanged);
            BaseDataView.CellClick += new DataGridViewCellEventHandler(this.BaseDataView_CellClick);
        }

        private void ReadV8iFile()
        {
            string PathV8i = @"%appdata%\1C\1CEStart\ibases.v8i";
            V8iContent = ReadFileContent(PathV8i);
            if (V8iContent is null)
            {
                MessageBox.Show("Не удалось найти файл ibases.v8i по указанному пути: " + PathV8i, "Ошибка чтения файла!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        private void LoadBasesToData()
        {
            // In case we are doing a reload
            baseData.dataTable.Rows.Clear();
            // Check if we have any source to work with
            if (V8iContent is null)
                return;
            // Fill Data
            int i = 0;
            var baseRow = new BaseRow();
            foreach (string line in V8iContent)
            {
                /* ---------- EXAMPLE
                [Торговля(Рабочая)]
                Connect=Srvr="winserv";Ref="ut2014_work";
                ID=f8370086-654e-4807-b79b-3f51653c3699
                OrderInList = 0
                Folder=/Рабочие
                OrderInTree = 0
                External=1
                ClientConnectionSpeed=Normal
                App = Auto
                WA=1
                Version=8.2
                DefaultApp=ThickClient
                -------------------- */
                if (line.StartsWith("[")) // Start
                {
                    baseRow.Name = line.Substring(1, line.Length - 2);
                    baseRow.Id = "";
                    baseRow.ConnectString = "";
                }
                else if (line.StartsWith("Connect=")) // Connect string
                {
                    baseRow.ConnectString = line.Substring(8);
                }
                else if (line.StartsWith("ID=")) // ID string
                {
                    baseRow.Id = line.Substring(3);
                }
                if (baseRow.Ready)
                {
                    baseRow.Index = i;

                    baseRow.FillSizesInRow();
                    baseRow.GetLockedAndUserInfo();

                    object[] row = { baseRow.Check, baseRow.Locked,
                        baseRow.Index, baseRow.Id, baseRow.Name, baseRow.ConnectString, baseRow.LastEnteredUser,
                        baseRow.SizeCacheUser,
                        baseRow.SizeCacheProgram,
                        baseRow.SizeTotal };
                    baseData.dataTable.Rows.Add(row);
                    // Clear
                    baseRow = new BaseRow();
                    baseRow.Check = false;
                    i++;
                }
            }
        }
        // TABLE INIT END
        // *********************************************************************************************        

        // *********************************************************************************************
        // EVENTS START
        public FormMain()
        {
            InitializeComponent();

            //Create DataTable with needed columns
            InitDataTable();
            InitDataGridView();
            // Read file into string[] collection
            ReadV8iFile();
            // Register keypressed event
            KeyPress += new KeyPressEventHandler(Control_KeyPressed);
            // CheckBoxes change
            cbOptAll.CheckedChanged += new System.EventHandler(this.CbOptClear_CheckedChanged);
            cbOptCacheUser.CheckedChanged += new System.EventHandler(this.CbOptClear_CheckedChanged);
            cbOptCacheProgram.CheckedChanged += new System.EventHandler(this.CbOptClear_CheckedChanged);
            cbOptSettingsUser.CheckedChanged += new System.EventHandler(this.CbOptClear_CheckedChanged);
            SetToolTips();
        }
        private void FormMain_Load(object sender, EventArgs e)
        {
            LoadBasesToData();
            //ToggleInfoView();
        }
        private void BaseDataView_CellFormatting(object sender, System.Windows.Forms.DataGridViewCellFormattingEventArgs e)
        {
            if (BaseDataView.Columns[e.ColumnIndex].DefaultCellStyle.Format.Equals("0.00"))
                if (e.Value.ToString().Equals("0"))
                    e.CellStyle.Format = "#.##";
        }
        /*
        private void BaseDataView_ValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (BaseDataView.Rows[e.RowIndex].DefaultCellStyle.BackColor == Color.LightCoral)
                return;

            bool isChecked = Convert.ToBoolean(BaseDataView.Rows[e.RowIndex].Cells[baseData.col_Check.ColumnName].Value);
            
            if (isChecked)
                BaseDataView.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.PaleGreen;
            else
                BaseDataView.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
        }
        */
        private void BaseDataView_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if(e.RowIndex >= 0)
            {
                bool isLocked = Convert.ToBoolean(BaseDataView.Rows[e.RowIndex].Cells[baseData.col_Locked.ColumnName].Value);
                if (isLocked)
                {
                    BaseDataView.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.DarkRed;
                    //BaseDataView.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.MistyRose;
                    BaseDataView.Rows[e.RowIndex].DefaultCellStyle.SelectionBackColor = Color.MistyRose;
                }

                bool isChecked = Convert.ToBoolean(BaseDataView.Rows[e.RowIndex].Cells[baseData.col_Check.ColumnName].Value);
                if (isChecked && !isLocked)
                    BaseDataView.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.PaleGreen;
                else if (!isLocked)
                    BaseDataView.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
            }
        }
        private void BaseDataView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
                ShowRowInfo();
        }
        private void Control_KeyPressed(object sender, KeyPressEventArgs e)
        {
            if (tbSearch.Focused)
                return;
            // toggle selection in datagrid on spacebar 
            if (e.KeyChar == ' ')
                BaseDataView.Focus();
                ToggleCheck_Selected();
        }
        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            BaseDataView_Refresh();
        }
        private void BtnSelectCache_Click(object sender, EventArgs e)
        {
            ToggleCheck(true, true);
        }

        private void BtnSelectAll_Click(object sender, EventArgs e)
        {
            ToggleCheck(true, false);
        }

        private void BtnUnselect_Click(object sender, EventArgs e)
        {
            ToggleCheck(false, false);
        }

        // Toggle InfoView
        private void CbInfo_CheckedChanged(object sender, EventArgs e)
        {
            ToggleInfoView();
        }

        // CheckBoxes
        private void CbOptClear_CheckedChanged(object sender, EventArgs e)
        {
            if (!cbOptAll.Checked && cbOptCacheUser.Checked && cbOptCacheProgram.Checked && cbOptSettingsUser.Checked)
            {
                cbOptAll.Checked = true;
                cbOptCacheUser.Checked = false;
                cbOptCacheProgram.Checked = false;
                cbOptSettingsUser.Checked = false;
            }

            cbOptCacheUser.Enabled = !cbOptAll.Checked;
            cbOptCacheProgram.Enabled = !cbOptAll.Checked;
            cbOptSettingsUser.Enabled = !cbOptAll.Checked;
        }
        // Search field
        private void TbSearch_TextChanged(object sender, EventArgs e)
        {
            Searh_ApplyFilter();
        }
        private void BtnDropSearch_Click(object sender, EventArgs e)
        {
            tbSearch.Text = "";
            Searh_ApplyFilter();
        }

        // About
        private void BtnAbout_Click(object sender, EventArgs e)
        {
            FormAbout aboutForm = new FormAbout();
            aboutForm.ShowDialog();
        }

        // Button Clear Cache
        private void BtnClear_Click(object sender, EventArgs e)
        {
            StartClear();
        }
        // EVENTS END
        // *********************************************************************************************

        // *********************************************************************************************
        // CLASSES START
        class BaseData
        {
            public DataTable dataTable;
            public DataColumn col_Check;
            public DataColumn col_Locked;
            public DataColumn col_Index;
            public DataColumn col_Id;
            public DataColumn col_BaseName;
            public DataColumn col_ConnectString;
            public DataColumn col_LastEnteredUser;
            public DataColumn col_SizeCacheUser;
            public DataColumn col_SizeCacheProgram;
            public DataColumn col_SizeTotal;
            public BaseData()
            {
                this.dataTable = new DataTable();

                this.col_Check = new DataColumn("В.", typeof(Boolean));
                this.col_Check.ReadOnly = false;
                this.col_Locked = new DataColumn("З.", typeof(Boolean));
                this.col_Locked.ReadOnly = true;

                // String columns
                this.col_Index = new DataColumn("#", typeof(Int32));
                this.col_Index.ReadOnly = true;
                this.col_Id = new DataColumn("ID", typeof(String));
                this.col_Id.ReadOnly = true;
                this.col_BaseName = new DataColumn("Наименование базы данных", typeof(String));
                this.col_BaseName.ReadOnly = true;
                this.col_ConnectString = new DataColumn("Строка подключения", typeof(String));
                this.col_ConnectString.ReadOnly = true;
                this.col_LastEnteredUser = new DataColumn("LastUser", typeof(String));
                this.col_LastEnteredUser.ReadOnly = true;
                // Numeric columns
                this.col_SizeCacheUser = new DataColumn("Кэш пользов.", typeof(Decimal));
                this.col_SizeCacheUser.ReadOnly = true;
                this.col_SizeCacheProgram = new DataColumn("Кэш прилож.", typeof(Decimal));
                this.col_SizeCacheProgram.ReadOnly = true;
                this.col_SizeTotal = new DataColumn("Всего", typeof(Decimal));
                this.col_SizeTotal.ReadOnly = true;


                DataColumn[] dataColumns = {
                col_Check,
                col_Locked,
                col_Index,
                col_Id,
                col_BaseName,
                col_ConnectString,
                col_LastEnteredUser,
                col_SizeCacheUser,
                col_SizeCacheProgram,
                col_SizeTotal
            };
                this.dataTable.Columns.AddRange(dataColumns);
            }
        }
        class BaseRow
        {
            public bool Check;
            public bool Locked;
            public int Index;
            public string Name;
            public string Id;
            public string ConnectString;
            public string LastEnteredUser;
            public decimal SizeCacheProgram;
            public decimal SizeCacheUser;
            public decimal SizeTotal;

            // for separate usage
            public List<string> rootFolders;
            public List<string> localCacheFolders;
            public List<string> userCacheFolders;

            public bool Ready => (!String.IsNullOrEmpty(this.Name) && !String.IsNullOrEmpty(this.Id) && !String.IsNullOrEmpty(this.ConnectString));

            public void FillSizesInRow()
            {
                string basePathLocal = Environment.ExpandEnvironmentVariables(@"%LOCALAPPDATA%\1C\1cv8\" + this.Id);
                string basePathRoaming = Environment.ExpandEnvironmentVariables(@"%APPDATA%\1C\1cv8\" + this.Id);
                this.rootFolders = new List<string>();
                this.localCacheFolders = new List<string>();
                this.userCacheFolders = new List<string>();
                if (Directory.Exists(basePathLocal))
                {
                    rootFolders.Add(basePathLocal);
                    string[] filterSearch = { "Config", "ConfigSave", "SICache", "vrs-cache" };
                    localCacheFolders.AddRange(GetFoldersById(basePathLocal, filterSearch));
                    if (localCacheFolders.Count() > 0)
                        foreach (string _Path in localCacheFolders)
                            this.SizeCacheProgram += DirSize(new DirectoryInfo(_Path), "");
                }
                if (Directory.Exists(basePathRoaming))
                {
                    rootFolders.Add(basePathRoaming);
                    string[] filterSearch = { "vrs-cache" };
                    userCacheFolders.AddRange(GetFoldersById(basePathRoaming, filterSearch));
                    if (userCacheFolders.Count() > 0)
                        foreach (string _Path in userCacheFolders)
                            this.SizeCacheUser += DirSize(new DirectoryInfo(_Path), "");
                }
                this.SizeTotal += DirSize(new DirectoryInfo(basePathLocal), "");
                this.SizeTotal += DirSize(new DirectoryInfo(basePathRoaming), "");

                // Format to Mb
                this.SizeCacheProgram = this.SizeCacheProgram / 1024 / 1024;
                this.SizeCacheUser = this.SizeCacheUser / 1024 / 1024;
                this.SizeTotal = this.SizeTotal / 1024 / 1024;
            }
            public void GetLockedAndUserInfo()
            {
                string basePathRoaming = Environment.ExpandEnvironmentVariables(@"%APPDATA%\1C\1cv8\" + this.Id);
                if (Directory.Exists(basePathRoaming))
                {
                    // Locked then file exists (1Cv8.lck)
                    this.Locked = File.Exists(basePathRoaming + "\\1Cv8.lck");
                    // Last entered user info (def.usr)
                    this.LastEnteredUser = "";
                    if (File.Exists(basePathRoaming + "\\def.usr"))
                    {
                        string[] lines = ReadFileContent(basePathRoaming + "\\def.usr");
                        if (lines != null)
                            if (lines.Count() > 0)
                                this.LastEnteredUser = lines[0].Substring(2, lines[0].Length - 4);
                    }
                }
            }
            private string[] GetFoldersById(string _Path, string[] _Filter)
            {
                List<string> foundFolders = new List<string>();
                foreach (string f in _Filter)
                {
                    foundFolders.AddRange(Directory.GetDirectories(_Path, f, SearchOption.AllDirectories).ToList());
                }
                return foundFolders.ToArray();
            }
        }
        class DeleteResults
        {
            public bool isError;
            public double sizeDeleted;
            public List<string> listErrors;
        }
        // CLASSES END
        // *********************************************************************************************
        
        // *********************************************************************************************
        // CLEAR START
        private void StartClear()
        {
            var checkedRows = from DataGridViewRow row in BaseDataView.Rows
                              where Convert.ToBoolean(row.Cells[baseData.col_Check.ColumnName].Value) == true
                              select row;
            if (checkedRows.Count() == 0)
            {
                MessageBox.Show("Ничего не выбрано.", "Meh...", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            if (!cbOptAll.Checked && !cbOptCacheUser.Checked && !cbOptCacheProgram.Checked && !cbOptSettingsUser.Checked)
            {
                MessageBox.Show("Не указано, что очищать.", "Meh...", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            if(cbOptAll.Checked || cbOptSettingsUser.Checked)
            {
                DialogResult dialogResult = MessageBox.Show("Это удалит все локальные настройки пользователя в выбранных базах. \r\nВы уверены, что хотите этого?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dialogResult != DialogResult.Yes)
                    return;
            }

            int successfullyDeleted = 0;
            int errorsOnDelete = 0;
            double totalSizeDeleted = 0;

            List<string> listSuccess = new List<string>();
            List<string> listError = new List<string>();
            List<string> listExceptions = new List<string>();

            foreach (DataGridViewRow row in checkedRows)
            {
                BaseRow baseRow = new BaseRow();
                baseRow.Id = row.Cells[baseData.col_Id.ColumnName].Value.ToString();
                baseRow.Name = row.Cells[baseData.col_BaseName.ColumnName].Value.ToString();
                baseRow.ConnectString = row.Cells[baseData.col_ConnectString.ColumnName].Value.ToString();

                baseRow.FillSizesInRow();
                baseRow.GetLockedAndUserInfo();

                if (cbOptAll.Checked) // Delete All
                {
                    DeleteResults deleteResults = DeleteFoldersContent(baseRow.rootFolders, "");
                    if (!deleteResults.isError)
                    {
                        successfullyDeleted++;
                        listSuccess.Add("[Полное] - " + baseRow.Name);
                    }
                    else
                    {
                        errorsOnDelete++;
                        listError.Add("[Полное] - " + baseRow.Name);
                        listExceptions.AddRange(deleteResults.listErrors);
                    }
                    totalSizeDeleted += deleteResults.sizeDeleted;
                }
                else
                {
                    if (cbOptCacheProgram.Checked) // Delete ProgramCache
                    {
                        DeleteResults deleteResults = DeleteFoldersContent(baseRow.localCacheFolders, "");
                        if (!deleteResults.isError)
                        {
                            successfullyDeleted++;
                            listSuccess.Add("[Кэш приложения] - " + baseRow.Name);
                        }
                        else
                        {
                            errorsOnDelete++;
                            listError.Add("[Кэш приложения] - " + baseRow.Name);
                            listExceptions.AddRange(deleteResults.listErrors);
                        }
                        totalSizeDeleted += deleteResults.sizeDeleted;
                    }
                    if (cbOptCacheUser.Checked) // Delete UserCache
                    {
                        DeleteResults deleteResults = DeleteFoldersContent(baseRow.userCacheFolders, "");
                        if (!deleteResults.isError)
                        {
                            successfullyDeleted++;
                            listSuccess.Add("[Кэш пользователя] - " + baseRow.Name);
                        }
                        else
                        {
                            errorsOnDelete++;
                            listError.Add("[Кэш пользователя] - " + baseRow.Name);
                            listExceptions.AddRange(deleteResults.listErrors);
                        }
                        totalSizeDeleted += deleteResults.sizeDeleted;
                    }
                    if (cbOptSettingsUser.Checked) // Delete UserSettings
                    {
                        DeleteResults deleteResults = DeleteFoldersContent(baseRow.rootFolders, "*.pfl");
                        if (!deleteResults.isError)
                        {
                            successfullyDeleted++;
                            listSuccess.Add("[Настройки пользователя] - " + baseRow.Name);
                        }
                        else
                        {
                            errorsOnDelete++;
                            listError.Add("[Настройки пользователя] - " + baseRow.Name);
                            listExceptions.AddRange(deleteResults.listErrors);
                        }
                        totalSizeDeleted += deleteResults.sizeDeleted;
                    }
                }
            }

            // Show results of operation
            FormResults resultForm = new FormResults();
            resultForm.SetResults(successfullyDeleted, errorsOnDelete, totalSizeDeleted,
                listSuccess, listError, listExceptions);
            resultForm.ShowDialog();

            BaseDataView_Refresh();
        }

        private DeleteResults DeleteFoldersContent(List<string> ListOfFolders, string filterMask)
        {
            DeleteResults deleteResults = new DeleteResults();

            deleteResults.isError = false;
            deleteResults.sizeDeleted = 0;
            deleteResults.listErrors = new List<string>();
            foreach (string folderPath in ListOfFolders)
            {
                double folderSize = Convert.ToDouble(DirSize(new DirectoryInfo(folderPath), filterMask));

                if (filterMask.Equals("")) // Clear by dirs
                {
                    try
                    {
                        Directory.Delete(folderPath, true);
                        deleteResults.sizeDeleted += folderSize;
                    }
                    catch (Exception e)
                    {
                        deleteResults.isError = true;
                        deleteResults.listErrors.Add(e.Message);
                    }
                }
                else // Clear by Files
                {
                    var filesFound = Directory.EnumerateFiles(folderPath, filterMask, SearchOption.AllDirectories);
                    foreach (var fi in filesFound)
                    {
                        try
                        {
                            new FileInfo(fi).Delete();
                            deleteResults.sizeDeleted += folderSize;
                        }
                        catch (Exception e)
                        {
                            deleteResults.isError = true;
                            deleteResults.listErrors.Add(e.Message);
                        }
                    }
                }
            }

            deleteResults.sizeDeleted = deleteResults.sizeDeleted / 1024 / 1024;
            return deleteResults;
        }

        // CLEAR END
        // *********************************************************************************************

        private void Searh_ApplyFilter()
        {
            try
            {
                string queryString = String.Format("[{1}] LIKE '%{0}%' OR [{2}] LIKE '%{0}%'",
                    EscapeLikeValue(tbSearch.Text), baseData.col_BaseName.ColumnName, baseData.col_ConnectString.ColumnName);
                baseData.dataTable.DefaultView.RowFilter = queryString;
            }
            catch { };
        }
        private void ToggleInfoView()
        {
            int currentSize = Size.Width;
            int expandWidth = new int[] { tbInfo.MinimumSize.Width, tbInfo.Size.Width }.Max() + 20;
            int collpaseWidth = splitMain.SplitterDistance + splitMain.SplitterWidth;

            splitMain.Panel2Collapsed = !cbInfo.Checked;
            cbInfo.Image = cbInfo.Checked ? Properties.Resources.arrows_16_L: Properties.Resources.arrows_16_R;

            int widthInfo = tbInfo.Size.Width;
            // Adapt sizing
            if (WindowState != FormWindowState.Maximized)
            {
                if(cbInfo.Checked) // Show
                {
                    Size = new System.Drawing.Size(currentSize + expandWidth, Size.Height);
                    splitMain.SplitterDistance = currentSize - splitMain.SplitterWidth;
                    ShowRowInfo();
                }
                else // Hide
                {
                    Size = new System.Drawing.Size(collpaseWidth, Size.Height);
                }
            }
        }
        private void ShowRowInfo()
        {
            if (splitMain.Panel2Collapsed)
                return;

            DataGridViewRow row = BaseDataView.CurrentRow;
            
            tbInfo.ResetText();
            if (row is null)
                return;
            BaseRow baseRow = new BaseRow();
            baseRow.Id = row.Cells[baseData.col_Id.ColumnName].Value.ToString();
            baseRow.FillSizesInRow();
            baseRow.GetLockedAndUserInfo();

            tbInfo.AppendText("--- Информация о базе ---");
            tbInfo.AppendText("\r\nИмя: " + row.Cells[baseData.col_BaseName.ColumnName].Value);
            tbInfo.AppendText("\r\nID: " + row.Cells[baseData.col_Id.ColumnName].Value);
            tbInfo.AppendText("\r\nСтрока подключения: " + row.Cells[baseData.col_ConnectString.ColumnName].Value);

            tbInfo.AppendText("\r\n");
            tbInfo.AppendText("\r\nЗаблокировано (база открыта?): " + (baseRow.Locked ? "Да" : "Нет"));
            tbInfo.AppendText("\r\nИмя пользователя при входе: " + (String.IsNullOrEmpty(baseRow.LastEnteredUser) ? "нет данных" : baseRow.LastEnteredUser));

            double _SizeCacheUser = Convert.ToDouble(row.Cells[baseData.col_SizeCacheUser.ColumnName].Value);
            double _SizeCacheProgram = Convert.ToDouble(row.Cells[baseData.col_SizeCacheProgram.ColumnName].Value);
            double _SizeTotal = Convert.ToDouble(row.Cells[baseData.col_SizeTotal.ColumnName].Value);
            tbInfo.AppendText("\r\n");
            tbInfo.AppendText("\r\nКэш пользователя: " + baseRow.SizeCacheUser.ToString("0.00") + " Mb");
            tbInfo.AppendText("\r\nКэш приложения: " + baseRow.SizeCacheProgram.ToString("0.00") + " Mb");
            tbInfo.AppendText("\r\nРазмер всего: " + baseRow.SizeTotal.ToString("0.00") + " Mb");

            tbInfo.AppendText("\r\n");
            tbInfo.AppendText("\r\nКорневые каталоги: ");
            foreach (string s in baseRow.rootFolders)
                tbInfo.AppendText("\r\n > " + s);

            tbInfo.AppendText("\r\n");
            tbInfo.AppendText("\r\nКаталоги кэша приложения: ");
            foreach (string s in baseRow.localCacheFolders)
                tbInfo.AppendText("\r\n > " + s);

            tbInfo.AppendText("\r\n");
            tbInfo.AppendText("\r\nКаталоги кэша пользователя: ");
            foreach (string s in baseRow.userCacheFolders)
                tbInfo.AppendText("\r\n > " + s);

            tbInfo.AppendText("\r\n");
        }
        private void BaseDataView_Refresh()
        {
            ReadV8iFile();
            LoadBasesToData();
            ShowRowInfo();
        }
        private static string[] ReadFileContent(string pathToFile)
        {
            string fullPathToFile = Environment.ExpandEnvironmentVariables(pathToFile);
            if (!File.Exists(fullPathToFile))
            {
                return null;
            }
            return System.IO.File.ReadAllLines(fullPathToFile);
        }
        private static long DirSize(DirectoryInfo d, string filterMask)
        {
            long size = 0;
            if (filterMask.Equals(""))
                filterMask = "*.*";
            // Add file sizes.
            if (!Directory.Exists(d.FullName))
                return size;
            var filesFound = Directory.EnumerateFiles(d.FullName, filterMask, SearchOption.AllDirectories);
            foreach (var fi in filesFound)
                size += new FileInfo(fi).Length;
            /*
            FileInfo[] fis = d.GetFiles();
            foreach (FileInfo fi in fis)
            {
                size += fi.Length;
            }
            // Add subdirectory sizes.
            DirectoryInfo[] dis = d.GetDirectories();
            foreach (DirectoryInfo di in dis)
            {
                size += DirSize(di, filterMask);
            }
            */
            return size;
        }
        private void ToggleCheck(bool checkState, bool cacheOnly)
        {
            foreach (DataGridViewRow row in BaseDataView.Rows)
            {
                if (checkState && cacheOnly) // Select only cache
                    row.Cells[baseData.col_Check.ColumnName].Value = Convert.ToDecimal(row.Cells[baseData.col_SizeCacheUser.ColumnName].Value) > 0;
                else if (!cacheOnly) // Select/deselect all
                    row.Cells[baseData.col_Check.ColumnName].Value = checkState;
            }
        }
        private void ToggleCheck_Selected()
        {
            var selectedRows = BaseDataView.SelectedRows;
            bool nextState = true;
            // Check the number of Cheked rows. If equals all - uncheck them
            int checkedRows = 0;
            foreach (DataGridViewRow row in selectedRows)
                if (Convert.ToBoolean(row.Cells[baseData.col_Check.ColumnName].Value))
                    checkedRows++;
            if (checkedRows == selectedRows.Count)
                nextState = false;
            // do the stuff
            foreach (DataGridViewRow row in selectedRows)
                row.Cells[baseData.col_Check.ColumnName].Value = nextState;
        }
        private static string EscapeLikeValue(string valueWithoutWildcards)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < valueWithoutWildcards.Length; i++)
            {
                char c = valueWithoutWildcards[i];
                if (c == '*' || c == '%' || c == '[' || c == ']')
                    sb.Append("[").Append(c).Append("]");
                else if (c == '\'')
                    sb.Append("''");
                else
                    sb.Append(c);
            }
            return sb.ToString();
        }
        private void SetToolTips()
        {
            ToolTip toolTip = new ToolTip();

            string ClearCacheUser_ToolTip = "Для выбранных баз - удаляет только файлы кэша пользователя.\r\nПользовательские настройки сохраняются.";
            string ClearCacheProgram_ToolTip = "Для выбранных баз - удаляет только файлы кэша приложения.\r\nПользовательские настройки сохраняются.";
            string ClearSettingsUser_ToolTip = "Для выбранных баз - удаляет только файлы настроек пользователя.\r\nФайлы кэша сохраняются.";
            string PurgeAll_ToolTip = "Для выбранных баз - удаляет все найденные в корневых каталогах файлы.\r\nПользовательские настройки будут удалены.";
            string SmartSelect_ToolTip = "Выбрать только базы, в которых есть локальный кэш пользователя.";
            string SelectAll_ToolTip = "Выбрать все базы в списке.";
            string RemoveSelection_ToolTip = "Убрать выбор со всех баз в списке.";

            string Search_ToolTip = "Поиск по совпадению в имени базы или в строке подключения.";
            string DropSearch_ToolTip = "Сбросить поиск.";
            string Refresh_ToolTip = "Перезаполнить список баз.";
            string Expand_ToolTip = "Показать\\Скрыть окно информации о выделенной строке.";

            string About_ToolTip = "О программе...";

            toolTip.SetToolTip(cbOptCacheUser, ClearCacheUser_ToolTip);
            toolTip.SetToolTip(cbOptCacheProgram, ClearCacheProgram_ToolTip);
            toolTip.SetToolTip(cbOptSettingsUser, ClearSettingsUser_ToolTip);
            toolTip.SetToolTip(cbOptAll, PurgeAll_ToolTip);
            toolTip.SetToolTip(btnSelectCache, SmartSelect_ToolTip);
            toolTip.SetToolTip(btnSelectAll, SelectAll_ToolTip);
            toolTip.SetToolTip(btnUnselect, RemoveSelection_ToolTip);

            toolTip.SetToolTip(tbSearch, Search_ToolTip);
            toolTip.SetToolTip(btnDropSearch, DropSearch_ToolTip);
            toolTip.SetToolTip(btnRefresh, Refresh_ToolTip);
            toolTip.SetToolTip(cbInfo, Expand_ToolTip);

            toolTip.SetToolTip(btnAbout, About_ToolTip);
        }
    }
}
